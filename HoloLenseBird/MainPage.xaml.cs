﻿using BirdUWP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.Storage.Pickers;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using System.Linq.Expressions;
using System.Reflection;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.ApplicationModel.ExtendedExecution;
using System.Runtime.InteropServices;
using Windows.Storage;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HoloLenseBird
{

    //public enum LogLines { ANY = -1, PKT_COUNT, INFO, WARN, ERR, STAT, CONN, DYNAMIC, TOTAL };
    public static class LOG_
    {
        struct Line
        {
            static int iter = 0;
            public int m_num;
            public int m_count;
            public Line(int num = 0, int count = 0)
            {
                m_num = iter++;
                m_count = count;
            }
            public void Init(int num = 0) { m_num = num; m_count = 0; }
        }
        static Line[] m_lines = new Line[(int)LogLines.TOTAL];

        static async Task<int> LogMsgA(LogLines ind, string prefix, string msg)
        {
            String logMsg = "";
            if (ind != LogLines.ANY)
            {
                m_lines[(int)ind].m_count++;
                logMsg = m_lines[(int)ind].m_count.ToString() + ") ";
            }
            logMsg += prefix + msg;
            int l = await MainPage.L(logMsg, ind);
            return l;
        }
        static async void LogMsg(LogLines ind, string prefix, string msg)
        {
            await LogMsgA(ind, prefix, msg);
        }
        public static void I(String msg)
        {
            LogMsg(LogLines.INFO, "", msg);
        }
        public static void E(String msg)
        {
            LogMsg(LogLines.ERR, "ERROR: ", msg);
        }
        public static void W(String msg)
        {
            LogMsg(LogLines.WARN, "WARNING: ", msg);
        }
        async public static Task<int> Na(String msg, int line)
        {
            return await LogMsgA((LogLines)line, "", msg);
        }
        public static void N(String msg, LogLines line)
        {
            LogMsg(line, "", msg);
        }
        public static string ARG<T>(Expression<Func<T>> expr)
        {
            var body = ((MemberExpression)expr.Body);
            string res = body.Member.Name + "=" + ((FieldInfo)body.Member).GetValue(((ConstantExpression)body.Expression).Value);
            res += (body.Member.Name + "=");
            return res;
        }
    }
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        bool TestOngoing { get; set; }
        BirdClient m_birdClient = new BirdClient(LogStr);// (LogStr);
        private readonly static SynchronizationContext m_syncContext = SynchronizationContext.Current;
        public static MainPage Me = null;
        string preferedDevNamePart = "1836";

        public MainPage()
        {
            InitializeComponent();
            //m_syncContext = SynchronizationContext.Current;
            Me = this;
            for (uint i = 5; i < 30; i += 2)
                sensitivityCombo.Items.Add(i);
            sensitivityCombo.SelectedItem = PktProcessor.Sensitivity;
            for (uint i = 0; i < (int)LogLines.TOTAL; i++)
                resList.Items.Add(CreateListItem(""));
            Application.Current.Suspending += new SuspendingEventHandler(App_Suspending);
        }
        private async void FileExp()
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = PickerLocationId.Downloads;// Directory.GetCurrentDirectory();
            picker.FileTypeFilter.Add(".dll");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                // Application now has read/write access to the picked file
                // System.Console.WriteLine(    "Picked photo: " + file.Name   ); 
            }
            else
            {
                //return "Operation cancelled.";
            }
        }
        string PrefFile { get { return "prefs.txt"; } }
        async void LoadPrefs()
        {
            try
            {
                Windows.Storage.StorageFolder storageFolder =
                    Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile prefFile =
                    await storageFolder.GetFileAsync(PrefFile);
                preferedDevNamePart = await FileIO.ReadTextAsync(prefFile);
            }
            catch (Exception ex)
            {
                LOG.E("LoadPrefs failure: " + ex.Message);
            }
        }
        async void WritePrefs()
        {
            try
            {
                StorageFolder storageFolder =
                    Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile prefFile =
                    await storageFolder.CreateFileAsync(PrefFile,
                        Windows.Storage.CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(prefFile, preferedDevNamePart);
            }
            catch (Exception ex)
            {
                LOG.E("LoadPrefs failure: " + ex.Message);
            }
        }
        public void LogPkt(string logStr)
        {
            LOG.N(logStr, LogLines.PKT_COUNT);
        }

        public AddedDevResponse AddedDevice(String devName)
        {
            bool isPrefered = false;
            try
            {
                isPrefered = (preferedDevNamePart.Length > 0 && devName.IndexOf(preferedDevNamePart) >= 0);
                m_syncContext.Post(new SendOrPostCallback(o =>
                {
                    portsCombo.Items.Add(devName);
                    if (isPrefered || portsCombo.Items.Count == 1)
                    {
                        portsCombo.SelectedIndex = (portsCombo.Items.Count - 1);
                    }
                }), null);
                return AddedDevResponse.CONTINUE;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Log DeviceInfoItem failure: " + ex.Message);
            }
            return AddedDevResponse.CONTINUE;
        }
        static async public Task<int> L(string logStrOrig, LogLines line = LogLines.ANY)
        {
            int retInd = -1;
            string logStr = DateTime.Now.ToString("hh:mm:ss.fff") + ": " + logStrOrig;
            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                retInd = LogStr(logStr, (int)line);
            });
            return retInd;
        }
        static TextBox CreateListItem(string str)
        {
            TextBox textBox = new TextBox();
            textBox.Text = str;
            textBox.TextWrapping = TextWrapping.Wrap;
            textBox.Width = Me.resList.Width;
            textBox.Margin = new Thickness { Left = -10, Top = -10, Bottom = -10 };
            textBox.BorderThickness = new Thickness(0, 0, 0, 0);
            //ToolTipService.SetToolTip(textBox, str);
            return textBox;
        }
        static int LogStr(string logStr, int ind = -1)
        {
            lock (MainPage.Me)
            {
                int retInd = ind;
                try
                {
                    if (String.IsNullOrEmpty(logStr))
                        logStr = "null";
                    Debug.WriteLine(logStr);
                    if (ind >= 0)// && ind < Me.resList.Items.Count)
                        m_syncContext.Post(new SendOrPostCallback(o =>
                        {
                            TextBox textBox = Me.resList.Items[ind] as TextBox;
                            if (textBox != null)
                            {
                                textBox.Text = logStr;
                                //ToolTipService.SetToolTip(textBox, logStr);
                            }
                            Me.resList.ScrollIntoView(Me.resList.Items[ind]);
                        }), null);
                    else
                        m_syncContext.Post(new SendOrPostCallback(o =>
                        {
                            TextBox textBox = CreateListItem(logStr);
                            retInd = Me.resList.Items.Count;
                            Me.resList.Items.Add(textBox);
                            Me.resList.ScrollIntoView(Me.resList.Items[retInd]);
                        }), null);
                    return retInd;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Log failure: " + ex.Message);
                    return retInd;
                }
            }
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            m_birdClient.Stop();
        }

        private void ButtonPorts_Click(object sender, RoutedEventArgs e)
        {
            LoadPrefs();
            m_birdClient.EnumerateDevices(AddedDevice);
        }

        private void ButtonExit_Click(object sender, RoutedEventArgs e)
        {
            m_birdClient.Stop();
            Application.Current.Exit();
        }
        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            //BackgroundWorker.Set(m_birdClient, portsCombo.SelectedItem.ToString());
            //bgWorkerStarter.RegisterAndRun();
            RunExtendedSession();
            //ConnectBird();
            //if (!m_birdClient.Connect(portsCombo.SelectedItem.ToString()))
            //    LOG.E("Cannot find device");
        }

        private void ButtonUnpair_Click(object sender, RoutedEventArgs e)
        {
            //BackgroundWorker.Set(m_birdClient, portsCombo.SelectedItem.ToString());
            //bgWorkerStarter.RegisterAndRun();
            if (portsCombo.SelectedItem != null)
                m_birdClient.Unpair(portsCombo.SelectedItem.ToString());
            //if (!m_birdClient.Connect(portsCombo.SelectedItem.ToString()))
            //    LOG.E("Cannot find device");
        }
        ExtendedExecutionSession extendedSess = null;
        async void App_Suspending(
        Object sender,
        Windows.ApplicationModel.SuspendingEventArgs e)
        {
            LOG.N("Application is suspended. Try to run in extended mode", LogLines.EXT_SESS);
            if (extendedSess != null)
            {
                LOG.N("Extended session already exists", LogLines.EXT_SESS);
                return;
            }
            extendedSess = new ExtendedExecutionSession();
            extendedSess.Reason = ExtendedExecutionReason.Unspecified;
            extendedSess.Revoked += SessionRevoked;
            ExtendedExecutionResult result = await extendedSess.RequestExtensionAsync();

            switch (result)
            {
                case ExtendedExecutionResult.Allowed:
                    LOG.N("Extended session is allowed", LogLines.EXT_SESS);
                    break;

                default:
                case ExtendedExecutionResult.Denied:
                    LOG.E("Cannot create extended session");
                    break;
            }
        }
        void ConnectBird()
        {
            if (portsCombo.SelectedItem != null)
                preferedDevNamePart = portsCombo.SelectedItem.ToString();
            else
                LoadPrefs();
            m_birdClient.Connect(portsCombo.SelectedItem == null ?
                                    preferedDevNamePart :
                                    portsCombo.SelectedItem.ToString());
        }
        async void RunExtendedSession()
        {
            if (portsCombo.SelectedItem != null)
                preferedDevNamePart = portsCombo.SelectedItem.ToString();
            else
                LoadPrefs();
            WritePrefs();
            if (extendedSess != null)
            {
                m_birdClient.Connect(portsCombo.SelectedItem == null ?
                        preferedDevNamePart :
                        portsCombo.SelectedItem.ToString());
            }
            else
            {
                extendedSess = new ExtendedExecutionSession();
                extendedSess.Reason = ExtendedExecutionReason.Unspecified;
                extendedSess.Revoked += SessionRevoked;
            }
            ExtendedExecutionResult result = await extendedSess.RequestExtensionAsync();

            switch (result)
            {
                case ExtendedExecutionResult.Allowed:
                    m_birdClient.Connect(portsCombo.SelectedItem == null ?
                        preferedDevNamePart :
                        portsCombo.SelectedItem.ToString());
                    break;

                default:
                case ExtendedExecutionResult.Denied:
                    LOG.E("Cannot create extended session");
                    break;
            }
        }
        private async void SessionRevoked(object sender, ExtendedExecutionRevokedEventArgs args)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                switch (args.Reason)
                {
                    case ExtendedExecutionRevokedReason.Resumed:
                        LOG.N("Extended execution revoked due to returning to foreground.", LogLines.EXT_SESS);
                        //ClearExtendedExecution(extendedSess);
                        break;

                    case ExtendedExecutionRevokedReason.SystemPolicy:
                        LOG.N("Extended execution revoked due to system policy.", LogLines.EXT_SESS);
                        break;
                }

                //EndExtendedExecution();
            });
        }
        void ClearExtendedExecution(ExtendedExecutionSession session)
        {
            if (session != null)
            {
                session.Revoked -= SessionRevoked;
                session.Dispose();
                session = null;
            }
        }
        private void sensitivityCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PktProcessor.Sensitivity = Convert.ToUInt32(e.AddedItems[0]);
        }

        private void GestCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            PktProcessor.GestureIsOn = true;
        }

        private void GestCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            PktProcessor.GestureIsOn = true;
        }
    }

}
